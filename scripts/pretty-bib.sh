#!/usr/bin/env bash

ROOTDIR=$(git rev-parse --show-toplevel)
BIB_FILES="${ROOTDIR}/bibs/*.bib"

for i in ${BIB_FILES}; do
    bibtool -r "${ROOTDIR}/bibtool.rsc" "$i" -o "$i"
done
