#!/usr/bin/env bash

ROOTDIR=$(git rev-parse --show-toplevel)
BIBSDIR="${ROOTDIR}/bibs/"
FILESDIR="${ROOTDIR}/files/"

TARGETDIR="/mnt/router/srv/http/lib/"

rsync -avzzu --delete "${BIBSDIR}" "${TARGETDIR}/bibs"
rsync -avzzu --delete "${FILESDIR}" "${TARGETDIR}/files"
